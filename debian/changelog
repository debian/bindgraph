bindgraph (0.2a-7) UNRELEASED; urgency=medium

  * debian/control:
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + New Maintainer (Closes: #838074)
    + Add Homepage, Vcs-Git, Vcs-Browser
    + Add B-D: dh-exec for lintian (script-with-language-extension)

  * debian/postinst:
    + Change RUNDIR (Closes: #301823)
    + Add #DEBHELPER#

  * debian/patches:
    + Add 0002-remove-subdir-for-one-pid-file.patch. PID is stored in an
      strange directory (Closes: #301823)
    + Drop bindgraph_cgi.patch. Applied upstream.
    + Drop bindgraph_pl.patch. Applied upstream.

  * lintian:
    + Drop unused overrides.
    + Fix script-with-language-extension
    + Fix maintainer-script-lacks-debhelper-token

 -- Juri Grabowski <debian@jugra.de>  Sat, 19 May 2018 14:07:28 +0200

bindgraph (0.2a-6) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group.  (See #838074)
  * Switch to debhelper compat level 11 with minimal dh rules.
  * Switch to Priority: optional.
  * Add bindgraph_startup.patch, thanks to Joseph Nahmias.  (Closes: #760660)
  * Remove /var/log/bind9-query.log on purge.  (Closes: #668730)
  * Fix some Lintian issues.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 16 Mar 2018 04:19:06 +0100

bindgraph (0.2a-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Modified debian/postinst to ensure the existence of the directory
    /var/run/servergraph before its configuration. Closes: #591601
  * upgraded Standards-Version to 3.9.1 and compat to 5.
  * replaced dh_clean -k by dh_prep in debian/rules.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 17 Aug 2010 16:29:24 +0200

bindgraph (0.2a-5) unstable; urgency=low

  * Packaging
    - Updated to S-V 3.8.3 with no changes
    - Upgraded to debhelper compatibility level 5
    - Converted to using source format "3.0 (quilt)"

  * Fixed initscript to declare correct LSB dependencies (Closes: #563786)

  * Upload sponsored by Petter Reinholdtsen.

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sat, 20 Mar 2010 21:17:38 +0100

bindgraph (0.2a-4) unstable; urgency=low

  * Acknowledge l10n NMU (thanks, Christian) (Closes: #491938)

  * Fix overwritting of user-defined config options
    on upgrade/reconfig (Closes: #503713)
    Preserve changes to /etc/default/bindgraph by applying those settings
    (if available) in debconf before prompting.

  * Set more strict permissions on /var/log/bind9-query.log if/when
    we create it (Closes: #490102)

  * Localization:
    - Dutch (Closes: #503742)

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sun, 16 Nov 2008 22:50:27 +0100

bindgraph (0.2a-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Swedish. Closes: #491938
    - Italian. Closes: #491938

 -- Christian Perrier <bubulle@debian.org>  Wed, 22 Oct 2008 20:24:13 +0200

bindgraph (0.2a-3) unstable; urgency=high

  * Acknowledge NMU
    - Updated to S-V 3.7.3 with no changes

  * Bindgraph.cgi already worked with RRDtool 1.2.x since 2a (Closes: #462407)

  * Reconfigure now updates logfile location (Closes: 318458)

  * Debconf translations
    - Vietnamese (Closes: #427204)
    - Danish (Closes: #427160)
    - Finish (Closes: #475388)
    - Traditional Chinese (Closes: #414729)

  * Remove debconf note 'configure_bind', considered to be abusive.

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sun, 09 Mar 2008 01:18:35 +0100

bindgraph (0.2a-2.1) unstable; urgency=high

  * Non-maintainer upload.
  * High-urgency upload for RC bugfix.
  * Guard the call to db_get in the postrm, to avoid failures in the case
    that debconf has already been removed from the system.  Closes: #416660.

 -- Steve Langasek <vorlon@debian.org>  Wed, 16 May 2007 21:13:58 -0700

bindgraph (0.2a-2) unstable; urgency=low

  * All localization fixes below prepared by Christian Perrier. Many thanks.

  * Fix errors in debconf templates. (Closes: #412620)

  * Debconf translations:
    - Galician. (Closes: #413359)
    - German. (Closes: #412619)
    - Czech. (Closes: #413344)
    - Russian. (Closes: #413547)
    - Brazilian Portuguese. (Closes: #413615)
    - Tamil. (Closes: #413823)
    - Japanese. (Closes: #413863)
    - Portuguese. (Closes: #413919)
    - Romanian. (Closes: #414052)
    - Spanish. (Closes: #414220)

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sun, 11 Mar 2007 14:57:44 +0100

bindgraph (0.2a-1) unstable; urgency=low

  * Too many fixes for just a regular "debian release" -> "a" release
    (no new upstream version yet)
    - Updated to Standards-Version 3.7.2 with no changes

  * bindgraph.cgi:
    - Updated to work with newer rrdtool(1.2.x) (Closes: #383490)
    - Updated to properly parse logfiles (Closes: #375237)

  * Postinst/postrm:
    - Fixed purge logic (Closes: #311177)

  * Depends:
    - Relaxed pre-depends on debconf to a regular "depends"

  * Internationalization / Localization
    - Brazilian portuguese (Closes: #373994)
    - Portuguese (corrected) (Closes: #381078)
    - Removed incorrect zh-tw (Closes: #403799)

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Fri,  8 Dec 2006 15:24:19 +0100

bindgraph (0.2-5) unstable; urgency=low

  * Acknowledge NMU. Thanks! (Closes: #354619)
    - Fix broken recommends (Closes: #353101).
    - Add debconf-2.0 alternative (Closes: #331761).
    - Remove /etc/default/bindgraph on purge (Closes: #327122).
    - Add Vietnamese debconf translation (Closes: #313156).

  * l10n:
    - Swedish translation (Closes: #331600)
    - Portuguese translation (Closes: #362982)

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Thu, 11 May 2006  1:32:26 +0200

bindgraph (0.2-4.1) unstable; urgency=high

  * Non-maintainer upload.
  * Fix broken recommends (Closes: #353101).
  * Add debconf-2.0 alternative (Closes: #331761).
  * Remove /etc/default/bindgraph on purge (Closes: #327122).
  * Add Vietnamese debconf translation (Closes: #313156).

 -- Luk Claes <luk@debian.org>  Mon, 27 Feb 2006 20:09:35 +0100

bindgraph (0.2-4) unstable; urgency=medium

  * Correct upgrade logic to include LOG_FORMAT=bind92 by default
    in Sarge [RC bug] (Closes: #297639)

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Fri, 01 Apr 2005 17:43:02 +0200

bindgraph (0.2-3) unstable; urgency=low

  * Several localization fixes:
    - Use templates corrected by Christian Perrier. Thanks!(Closes: #295177)
    - Added Brazilian Portuguese (Closes: #297056)

  * Correct minor typo in CGI (Closes: #298876)

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Tue, 01 Mar 2005 00:37:43 +0100

bindgraph (0.2-2) unstable; urgency=low

  * Added BIND 9.2/9.3 format support to maintainer and init scripts.
    Default for Sarge is BIND 9.2 (Closes: #293819)

  * Added upgrade support 0.1 -> 0.2 (log_format)

  * Added localization: Dutch (Closes: #293970)

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sun, 06 Feb 2005 12:54:26 +0100

bindgraph (0.2-1) unstable; urgency=low

  * New upstream version

  * Several debconf translations. Thanks to everybody!
    - French, by Clement Stenac(do include it this time!) (Closes: #268157)
    - Japanese translation, added once again (Closes: #269002)
    - Traditional Chinese (Closes: #293090)
    - Czech (Closes: #293190)
    - Danish (Closes: #293205)

  * Change to relative URLs, suggested by <weasel@debian.org> (Closes: #270264)

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Thu, 03 Feb 2004 21:31:26 +0100

bindgraph (0.1-2) unstable; urgency=low

  * Silly mistake in long description, fixed (Closes: #268065)
  * French debconf translation, by Clément Stenac (Closes: #268157)

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Wed, 29 Sep 2004 20:00:26 +0200

bindgraph (0.1-1) unstable; urgency=low

  * Fullfill ITP (Closes: #256537)
    - Packaged BindGraph 0.1
    - PO-Debconf-based configuration.
    - Several changes to upstream code to make it more user friendly

  * Package sponsored by Roberto Lumbreras <rover@debian.org>

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sun, 01 Aug 2004 23:08:26 +0200
